## Free Space Signal Communication

[![DOI](https://img.shields.io/badge/DOI-10.31219%2Fosf.io%2Fbmp89-green.svg)](https://doi.org/10.31219/osf.io/bmp89)

**Breif Concept/Idea.**
Coceptualized around  in the year `2017`

**When Signals Communicate in Free Space?** - A free Space Signal Communication.

Concept of exchanging the information between the Signal(s) when they interact in free space. 

![FreeSpaceCommunication](uploads/9d3ce2c8c313407c65c5a1b5f139207e/FreeSpaceCommunication.jpg)

**Terminology:** Free-Space indicate a Wired Environment or a Wireless Environment in the context of Information exchange.

In general signal(s) travel form Transmitting Node (Tx) to the Receiving Node (Rx), as most of us knows. What if two signals (One Signal from Tx and other Signal form Rx ) interact in the Free-Space and exchange the information in free space rather than processing the information at the systems ends.


This generally reduces the complexity of present day systems. As the information is exchanged/communicated between signals in space(Free Space), system resources can be used for other purpose, such as to analyze the information and to increase its graphical interface. In-fact space signal communication would greatly increases the speed of communication. About `20% - 30%` of time is saved in exchanging/processing the information between two nodes when compared with the existing system.

More over this increases the Physical Layer[PHY] capabilities. This era of AI, where lower layer such as Physical Layer must be smart. So there is also need for increasing the Physical Layer[PHY] smartness by increasing the efficiency of Physical Layer Protocols.

**Frame**: `Encapsulated Unique Key/ID` + `Encapsulated Information` + `Encapsulated Flag Code` .

1. Communication(and Authentication )  Establishment:
    * Tx : Unique Hashing Key is generated.

    * Tx ---> Rx

    * Unique Hashing Key is encoded and encapsulated with Transmitter (Tx) Signal, and Transmitted to the designated Receiver (Rx) with IP System over Secure Channel .
    * Rx ---> Tx

2. During Communication : Exchange of Information.
     *   Both Nodes, Tx and Rx starts transmitting Signals where Signal from Tx has Encoded Information and the Signal from Rx has `Null Information` encoded in empty bits. 

     *   **Simply**: Tx node signal has `Unique Hashing Key/Id` along with `Some Information` and Rx node Signal with `Unique Hashing Key/ID` with `no` additional information but with `empty bits`, to receive the exchanged information from Tx.

      * Tx `<--->` Free Space `<--->` Rx


3. Termination:
   * Communication is Terminated when TX Node signal exchanges `Termination` `Flag` with RX node Signal in Free Space.
   * Unique Hashing Key/Id at the system ends will be diminished! There by no trace of system information is logged. And new `Unique Hasihing Key/ID`'s is generated when and when exchange of information is needed.
Terminology:

**Obligations/Realtime/Phy requirements:**

1. Signals should have capability to process the information in the Free Space, which means to have inheritance charateristics of Antennas(Receiving and Transmitting).
2. Should posses electrons with more energy, for exchanging the information through electrons by exchanging the electrons of Tx Node Signal with electrons of Rx nOde Signal with electrostatic transfer of information. 
3. Here the exchange indicates the processing the information, which will reduce the complexity of end systems to analyze the received information at Physical Layer.


## Discussions:
  1. Major issue is to make electrons communicate/Talk to each other in Free Space, After research on how we can, There is a possibility of happening it through `Quantum Communication`-`Through Electron Spin Talk` as mentioned in - [Electron spins talk to each other via a 'quantum mediator'](#1-electron-spins-talk-to-each-other-via-a-quantum-mediator-httpsphysorgnews2016-10-electron-quantumhtml-) .
  



## Resource/References:
 ### 1. `Electron spins talk to each other via a 'quantum mediator'` - https://phys.org/news/2016-10-electron-quantum.html .
 ### 2. `Coherent spin-exchange via a quantum mediator` *Timothy Alexander Baart, Takafumi Fujita, Christian Reichl, Werner Wegscheider & Lieven Mark Koenraad Vandersypen* https://doi.org/10.1038/nnano.2016.188


------------------------------------------------------------------------------------------------------

### Brief Concept and its Update  at Project [wiki](https://gitlab.com/gorlapraveen/free-space-signal-communication/wikis/home)